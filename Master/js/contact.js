$(function () {
    $('a[href="#contact"]').on('click', function(event) {
        event.preventDefault();
        $('#contact').addClass('open');
        $('#contact > form > input[type="contact"]').focus();
    });
    
    $('#contact, #contact button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
    $('form').submit(function(event) {
        event.preventDefault();
        return false;
    })
});