<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Colombo Friend-In-Need Society</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
   	<link href="slider/js-image-slider.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
   	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>

<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>
</head>
<body>
<div><?php include 'header.php'; ?></div>
	<!-- Start -->
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="1000">
    
    <ol class="carousel-indicators">
        <li data-target="site-carousel" data-slide-to="0" class="active"></li>
        <li data-target="site-carousel" data-slide-to="1"></li>
        <li data-target="site-carousel" data-slide-to="2"></li>
        <li data-target="site-carousel" data-slide-to="3"></li>
    </ol>
    
    <div class="carousel-inner">
        
        <div class="item active">
            <img src="slider/slider/2.jpg">
            <div class="container">
                <div class="carousel-caption">
                    <h2>Discover the World at Your Doorstep</h2>
                    <p>National School of Business Mangement</p>
                </div>
            </div>  
        </div>
        <div class="item">
            <img src="slider/slider/1.jpg">
            <div class="container">
                <div class="carousel-caption">
                    <h2>WORLD-CLASS BUSINESS LEADERSHIP AND TECHNOLOGICAL INNOVATION</h2>
                    <p>National School of Business Management (NSBM) </p>
                </div>
            </div>  
        </div>
        <div class="item">
            <img src="slider/slider/2.jpg">
            <div class="container">
                <div class="carousel-caption">
                    <h2>Discover the World at Your Doorstep</h2>
                    <p>National School of Business Mangement</p>
                </div>
            </div>  
        </div>
        <div class="item">
            <img src="slider/slider/1.jpg">
            <div class="container">
                <div class="carousel-caption">
                    <h2>Discover the World at Your Doorstep</h2>
                    <p>National School of Business Mangement</p>
                </div>
            </div>  
        </div>
    
    </div>

    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
    
</div><div style="clear:both;"></div>

    <!---->
            <div class="main-text hidden-xs">
                <div class="col-md-12 text-center">
                    <h1>
                        The Colombo Friend-In-Need Society</h1>
                    <h3>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </h3>
                    
                </div>
            </div>
<div id="push">
</div>
<br>


<!-- *****************************Logo**************************************************-->
<div align="center">
	<img class="img-responsive" src="img/HD.jpg" alt="">
</div>

<!-- *****************************section-one**************************************************-->

   



<div class="information hidden-xs">
<!-- Donation Required -->
	<div class="container ">
		<div class="col-md-12">
			<div class="col-md-8">
				<div class="row">
					<div class="col-xs-18">
						<div class="media-body">
							<h4>Donation Required</h4>
						</div>
						<h5 style="color: green;">Donation Required List</h5>
						<hr align="left" width="100%">
					</div>
				</div>
			</div>
			
			<div class="col-md-4 visible-lg">
				<div class=".media .pull-left">
					<img src="img/P001.jpg" class="img-responsive" alt="Responsive image">
				</div>
			</div>

	

		</div>

	</div>
	<hr>
	<!-- Success Stories -->
	<div class="container ">
		<div class="col-md-12">
			<div class="col-md-8">
				<div class="row">
					<div class="col-xs-18">
						<div class="media-body">
							<h4>Successful Stories</h4>
						</div>
						<h5 style="color: green;">Successful Stories</h5>
						
					</div>
				</div>
			</div>
			
			<div class="col-md-4 visible-lg">
				<div class=".media .pull-left">
					<img src="img/P002.jpg" class="img-responsive" alt="Responsive image">
				</div>
			</div>

	

		</div>

	</div>


    <div class="clearfix"></div>

<div style="clear:both;"></div>
<!-- end information -->

<!-- successful stories -->

<!-- End of successful stories -->

	
</div><!-- end information -->
 	
<!-- *****************************info**************************************************-->
<div class="discover">

	<div style="clear:both;"></div>
</div>
	<!-- End -->
<style type="text/css">


</style>
	<style type="text/css">
		.main-text
{
    position: absolute;
    top: 50px;
    width: 96.66666666666666%;
    color: #FFF;
}
.btn-min-block
{
    min-width: 170px;
    line-height: 26px;
}
.btn-clear
{
    color: #FFF;
    background-color: transparent;
    border-color: #FFF;
    margin-right: 15px;
}
.btn-clear:hover
{
    color: #000;
    background-color: #FFF;
}
</style>



<?php //footer
include 'footer.php';
?>  
</body>
</html>